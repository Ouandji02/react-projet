import React, { useEffect, useState } from 'react';
import ReactDom from 'react-dom';
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import './index.css';
import { div } from 'prelude-ls';
import { ButtonGroup, Input } from '@material-ui/core';
import { Container } from '@material-ui/core';
import  AcUnitOutlinedIcon  from '@material-ui/icons/AcUnitOutlined';
import  SearchIcon  from '@material-ui/icons/SearchOutlined';
//import  AlertIcon  from '@material-ui/icons/AlertOutlined';
import  IconButton  from '@material-ui/core/IconButton';
import  SendIcon  from '@material-ui/icons/Send';
import { makeStyles } from '@material-ui/core';
import { TextField } from '@material-ui/core';
function useAutoIncremante(initial=0, step=1){
   const [countAuto,setStateAuto] = useState(initial)
   const AutoInscremente = function (){
      setStateAuto(countAuto => countAuto+step)
      
   }
   useEffect(function () {
      
      const timer = window.setInterval(AutoInscremente,1000)
      return function(){
         clearInterval(timer)
      }
   },[])
   
   return countAuto
}
function useIncremante(initial = 0, step = 1){
         const [count,setState] =useState(initial)
        
         function incrementer(){
            setState(c => c+step)
         } 
        
         return [count,incrementer]
     }

 function Affiche(initial){
    const [checked,UseStat] = useState(initial)
    function toggle(){
            UseStat(!checked)
         }
         return [
            checked,
            toggle
         ]
 }
function Home() {
        const [count,incrementer]=useIncremante(10,5) 
        const [checked,toggle] = Affiche(false)
        let countAuto=useAutoIncremante(0,10)
        
        
       
        return <Container>
                     <div>
                       <input type="checkbox" id='print' value={checked} onChange={toggle} /> <label htmlFor="print">Afficher le compteur</label> 
                     </div>
                     {
                        checked ? <button >incrementer{countAuto}</button> : null
                     }
                     

                </Container>
    
}
ReactDom.render(<Home/>,document.querySelector('#root'))
