import React, { useEffect, useState } from 'react';
import ReactDom, { render } from 'react-dom';
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import './index.css';
import { div } from 'prelude-ls';
import { ButtonGroup, Input } from '@material-ui/core';
import { Container } from '@material-ui/core';
import AcUnitOutlinedIcon from '@material-ui/icons/AcUnitOutlined';
import SearchIcon from '@material-ui/icons/SearchOutlined';
//import  AlertIcon  from '@material-ui/icons/AlertOutlined';
import IconButton from '@material-ui/core/IconButton';
import SendIcon from '@material-ui/icons/Send';
import { makeStyles } from '@material-ui/core';
import { TextField } from '@material-ui/core';
const useStyle = makeStyles({
    principal: {
        marginTop: "15%",
        border: "7px solid black",
        padding: "10px 0"
    },
    heure: {
        padding: '5px',
        backgroundColor: "black",
        color: "white",
        margin: '30px 10px',
        userSelect: 'none',
        boxShadow: '1px 2px 1px black'
    }

})

function Heure() {
    
    const Datep = new Date()
    const classes = useStyle()

    const [heure, setState] = useState({
        heure : Datep.getHours(),
        Minutes : Datep.getMinutes(),
        Secondes : Datep.getSeconds(),
    })

    useEffect(()=>{
        const event = new Date()
        const heure = document.querySelector('input').value ? document.querySelector('input').value : event.getHours()
        const minute = document.querySelector('#minute').value ? document.querySelector('#minute').value : event.getMinutes()
        if(heure > 24 ){
            document.querySelector('input').value = null
        }
        if(minute > 59 ){
            document.querySelector('#minute').value = null
        }

        setState({
            heure : heure,
            Minutes : minute,
            Secondes : event.getSeconds()
        })
    },[heure])

    let MomentHeure = heure.heure.toString().padStart(2, '0')
    const momentMinute = heure.Minutes.toString().padStart(2, '0')
    const momentSecond = heure.Secondes.toString().padStart(2, '0')

  

    const JourAbbre = Datep.toDateString().split(' ')
    let jour = ''

    let MomentJournee = ''

    if (parseInt(MomentHeure) > 12 && parseInt(MomentHeure) < 18) {
        MomentJournee = 'Apres Midi'
    } else if (parseInt(MomentHeure) > 17) {
        MomentJournee = 'Soir'
    } else {
        MomentJournee = 'Matin'
    }


    switch (JourAbbre[0]) {
        case 'Mon':
            jour = 'Lundi'
            break;
        case 'Tue':
            jour = 'Mardi'
            break;
        case 'Wed':
            jour = 'Mercredi'
            break;
        case 'Thu':
            jour = 'Jeudi'
            break;
        case 'Fri':
            jour = 'Vendredi'
            break;
        case 'Sat':
            jour = 'Samedi'
            break;
        default:
            jour = 'Dimanche'

    }

    let date = jour + ' ' + JourAbbre[2] + ' ' + JourAbbre[1] + ' ' + JourAbbre[3]
    
    
    
    return <>
        <Typography variant='h3' align="center">{jour}</Typography>
        <Typography variant='h4' align="center">{MomentJournee}</Typography>
        <Typography variant='h3' align="center" style={{ marginTop: '10px', marginBottom: '10px' }}><span id="heure" className={classes.heure} >{MomentHeure}</span>:<span className={classes.heure}>{momentMinute}</span>:<span className={classes.heure}>{momentSecond}</span></Typography>
        <Typography variant='h5' align="center">{date}</Typography>
        <Typography variant='h6' color="primary" align="left" style={{userSelect: 'none',marginLeft: "10px",}} >Modifier</Typography>
        <Typography align='center'> <input type="number" placeholder="Changer heure" align='center' id="heure"/> <input type="number" id="minute" placeholder="Changer minute"/><input type="number" id="seconde" placeholder="Changer seconde"/> </Typography>
     </>
}

export function Main() {
    const classes = useStyle()

    return <Container maxWidth='xs' className={classes.principal}>

        <div>
            <Heure />
        </div>

    </Container>
}

ReactDom.render(
    <Main />,
    document.querySelector('#root')
)
