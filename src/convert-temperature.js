import React from 'react';
import ReactDom from 'react-dom';
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import './index.css';
import { div } from 'prelude-ls';
import { ButtonGroup } from '@material-ui/core';
import { Container } from '@material-ui/core';
import  AcUnitOutlinedIcon  from '@material-ui/icons/AcUnitOutlined';
import  AlarmIcon  from '@material-ui/icons/AddAlarmOutlined';
//import  AlertIcon  from '@material-ui/icons/AlertOutlined';
import  IconButton  from '@material-ui/core/IconButton';
import  SendIcon  from '@material-ui/icons/Send';
import { makeStyles } from '@material-ui/core';
import { TextField } from '@material-ui/core';

function TemperatureVerdict ({celsius}) {
      if(celsius>=100){
         return <Container>
         <br/><div class="rouge">
            l'eau boue
         </div>
      </Container> 
      }else{
         return <Container>
                  <br/><div className="vert">
                     l'eau ne boue pas
                  </div>
               </Container> 
      }
   }
class Calculator extends React.Component{
   constructor(props){
      super(props)
      this.state ={degre: '', farenheit:''}
   }
   handleChange(){
      const valueFarenheit = document.querySelector('#degre').value ?  document.querySelector('#degre').value*1.8+32 : ''
      this.setState({
         degre: document.querySelector('#degre').value,
         farenheit:valueFarenheit
      })
   }
   handleChanges(){
      const valueDegre = document.querySelector('#farenheit').value ?  document.querySelector('#farenheit').value/1.8-32 : ''
      this.setState({
         degre: valueDegre,
         farenheit:document.querySelector('#farenheit').value
      })
   }
   
      
   
   render(){
      return<Container color="black">
               <Container>
                     <Typography
                        align="center"
                        variant="h1"
                     >
                         <div class="title">Hello world</div>
                     </Typography>
                    
                     <Typography
                        align="center"
                        variant="h6"
                        color="secondary"
                     ><Container >
                        Bienvenue sur le Convertisseur qui vous permet de convertir la temperature de 
                        degre celsius en degre farenheit et d'afficher juste en dessous l'ebullition de l'eau
                        à cette temperature.
                     </Container>
                        
                     </Typography>
                     
                     <Typography 
                     align="center"
                     display="inline"
                     class="form"
                     
                     >
                     
                     <Container class="input1">
                        <TextField type="number" variant="outlined" textColor="secondary" color="secondary" id="degre" value={this.state.degre} onChange={this.handleChange.bind(this)} label="Entrer la temperature en degre" height="40px"/>
                     </Container>    
                     <Container class="input2">
                        <TextField type="number" variant="outlined" color="secondary" id="farenheit" value={this.state.farenheit} onChange={this.handleChanges.bind(this)} label="Entrer la temperature en farenheit"/>
                     </Container>
                          
                     </Typography>
                     <Typography
                      align="center"
                     >
                        {
                           this.state.degre ? <TemperatureVerdict celsius={this.state.degre}/> : null
                        }
                     </Typography>
                     
               </Container>
            
            </Container>  
            }
} 
ReactDom.render(<Calculator/>,document.querySelector('#root'))
 