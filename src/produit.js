import React from 'react';
import ReactDom from 'react-dom';
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import './index.css';
import { div } from 'prelude-ls';
import { ButtonGroup, Input } from '@material-ui/core';
import { Container } from '@material-ui/core';
import  AcUnitOutlinedIcon  from '@material-ui/icons/AcUnitOutlined';
import  SearchIcon  from '@material-ui/icons/SearchOutlined';
//import  AlertIcon  from '@material-ui/icons/AlertOutlined';
import  IconButton  from '@material-ui/core/IconButton';
import  SendIcon  from '@material-ui/icons/Send';
import { makeStyles } from '@material-ui/core';
import { TextField } from '@material-ui/core';
var produit =[
    {category:"sport",name:"ballon",prix:"15000",stocked:true},
    {category:"sport",name:"foot",prix:"15000",stocked:true},
    {category:"sport",name:"basket",prix:"15000",stocked:true},
    {category:"sport",name:"sifflet",prix:"15000",stocked:false},
    {category:"sport",name:"maillot",prix:"15000",stocked:true},
    {category:"electronic",name:"courant",prix:"15000",stocked:true},
    {category:"electronic",name:"RAM",prix:"15000",stocked:false},
    {category:"electronic",name:"disque dur",prix:"15000",stocked:true},
    {category:"electronic",name:"ecouteur",prix:"15000",stocked:true},
    {category:"electronic",name:"prise",prix:"25000",stocked:true},
    {category:"Mathematique",name:"excellence",prix:"25000",stocked:true}

]

function recherche({valeur},{product}){
    
         
}

class Header extends React.Component{
    constructor(props){
        super(props)
        
    }
    handleChange(e){
        if(e.target.type=='checkbox'){
            this.props.handleChangeStock(e.target.checked)    
        }else{
            this.props.handleChangeText(e.target.value)
        }
        
        
    }
    
    render(){
        const {value,stock}=this.props
         return <form action="" align="center">
                    <TextField id="textfield"  label="Entrer le nom du produit" value={value} variant="outlined" onChange={this.handleChange.bind(this)}/>
                    <div class='checkbox' align="left" style={{marginLeft:'10px ',fontSize:'21px', paddingTop:'10px'}}><input type="checkbox" checked={stock} id="vider" onChange={this.handleChange.bind(this)}/><label htmlFor="vider"> Afficher les produits en stock seulement</label></div>
                </form>
    }
}

function ProductRow({product,valeur,stock}){
    let   name = null
    if(valeur == ''){
        name=<tr>
        <td>{product.name}</td>
        <td>{product.prix}</td>
    </tr> 
    }else{
        if(product.name== valeur.toLowerCase() || !product.name.indexOf(valeur)){
            name = <tr>
            <td>{product.name}</td>
            <td>{product.prix}</td>
        </tr> 
        }else{
            name = null
        }
    }
    return <>{
        name
        
    }
    </>
}

function CategoryRow({category}){
    return <tr>
        <th colSpan="2">
            {category}
        </th>
    </tr>
}

function Body({product,value,stock}){
    var row=[]
    var lastCategoty = ''
    product.forEach(element => {
        
        if(element.category!=lastCategoty){
            lastCategoty=element.category
            row.push(<CategoryRow category={lastCategoty}/>)
        }
        if(stock){
          if(!element.stocked){
            return 
          }  else{
                 row.push(<ProductRow product={element} valeur={value}/>)
             } 
        }else{
            row.push(<ProductRow product={element} valeur={value}/>)
        }
       
        
    });
    return <Container>
        <table border="1">
                
                <thead>
                   
                    <tr>
                        <th>name</th>
                        <th>prix</th>
                    </tr>
                </thead>
                <tbody>
                    {row}
                </tbody>
        </table>
    </Container>
}
class  Principals extends React.Component{
    constructor(props){
        super(props)
        this.state={value:'ballon',
                    stock:false    
                }
    }
    handleChangeText(value){
        this.setState({value})
    }
    handleChangeStock(check){
        this.setState({stock:check})
    }
    render(){
        
         return  <Container>
                    <Header value={this.state.value}
                            stock={this.state.stock}
                            handleChangeText={this.handleChangeText.bind(this)}
                            handleChangeStock={this.handleChangeStock.bind(this)} />
                    <Body
                        product={produit}
                        value={this.state.value}
                        stock={this.state.stock}
                        />
                </Container>
    }
   
    
}

export default Principals